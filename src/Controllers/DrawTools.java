package Controllers;
/**
 * All the possible drawing modes (tools).
 */
public enum DrawTools {
    PLOT,
    LINE,
    RECTANGLE,
    ELLIPSE,
    POLYGON
}

package Controllers;

import GUI.VecPaintGUI;
import VecPaint.VecPaint;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * It essentially adds functionality to the GUI.
 * It does this by creating the required Listeners for the GUI and linking them to behaviours defined in the model.
 */
public class GuiController {
    private VecPaintGUI gui;
    private VecPaint model;
    private Point start;
    private Point end;
    private ArrayList<Point> polyPoints = new ArrayList<>();
    private Color penColor = Color.BLACK;
    private Color fillColor = Color.WHITE;

    private JFileChooser fc = new JFileChooser();
    private FileNameExtensionFilter vecFilter = new FileNameExtensionFilter("VecPaint File", "vec");


    /**
     * @param gui the GUI Object
     * @param vecPaint the model Object
     */
    public GuiController(VecPaintGUI gui, VecPaint vecPaint){
        model = vecPaint;
        fc.addChoosableFileFilter(vecFilter);
        fc.setFileFilter(vecFilter);

        gui.openOption.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int returnVal = fc.showOpenDialog(gui.window);
                if(returnVal== JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    model.open(file.toPath());
                    //String filename = file.getAbsolutePath();
                } else if(returnVal==JFileChooser.CANCEL_OPTION) { }
            }
        });

        gui.saveOption.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int returnVal = fc.showOpenDialog(gui.window);
                if(returnVal== JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    try {
                        model.save(file);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    //String filename = file.getAbsolutePath();
                } else if(returnVal==JFileChooser.CANCEL_OPTION) { }
            }
        });

        gui.penColourButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Color initialcolor=penColor;
                penColor = JColorChooser.showDialog(gui.window,"Select a color",initialcolor);
                if (penColor == null){penColor = initialcolor;}

                gui.penColorTile.setBackground(penColor);
                model.addColorCommand("PEN", penColor);
            }
        });

        gui.fillColorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Color initialcolor=fillColor;
                fillColor = JColorChooser.showDialog(gui.window,"Select a color",initialcolor);
                if (fillColor == null){fillColor = initialcolor;}

                gui.fillColorTile.setBackground(fillColor);
                if (gui.fillCheckBox.isSelected()){
                        model.addColorCommand("FILL", fillColor);
                }
            }
        });

        gui.fillCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (!gui.fillCheckBox.isSelected()){
                    model.fillOff();
                }
                else {
                    model.addColorCommand("FILL", fillColor);
                }
            }
        });


        gui.plotButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                gui.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
                model.setDrawTool(DrawTools.PLOT);
            }
        });

        gui.lineTool.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gui.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
                model.setDrawTool(DrawTools.LINE);
            }
        });

        gui.rectangleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gui.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
                model.setDrawTool(DrawTools.RECTANGLE);
            }
        });

        gui.ellipseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gui.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
                model.setDrawTool(DrawTools.ELLIPSE);
            }
        });

        gui.polygonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gui.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
                model.setDrawTool(DrawTools.POLYGON);
            }
        });

        gui.polygonButton.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (polyPoints.size()>1 && e.getKeyCode()==KeyEvent.VK_SPACE){
                    polyPoints.add(polyPoints.get(0));
                    Point[] points = new Point[polyPoints.size()];
                    for (int i = 0; i < polyPoints.size(); i++) {
                        points[i] = polyPoints.get(i);
                    }
                    model.addCommand("POLYGON", points);
                    polyPoints.clear();
                    gui.infoLabel.setText(" ");
                }
                else if (e.getKeyCode()==KeyEvent.VK_ESCAPE){
                    polyPoints.clear();
                    gui.infoLabel.setText(" ");
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });


        gui.getCanvas().addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (model.getDrawTool()==DrawTools.PLOT){
                    Point[] points = {e.getPoint()};
                    model.addCommand("PLOT", points);
                }
                else if (model.getDrawTool()==DrawTools.POLYGON){
                    polyPoints.add(e.getPoint());
                    gui.infoLabel.setText("SPACE to finish shape, ESC to cancel");
                }

                }

            @Override
            public void mousePressed(MouseEvent e) {
                    start = e.getPoint();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                end = e.getPoint();
                Point[] points = {start, end};

                if (model.getDrawTool()==DrawTools.LINE){
                    model.addCommand("LINE", points);
                }
                else if (model.getDrawTool()==DrawTools.RECTANGLE){
                    PointCheck();
                    model.addCommand("RECTANGLE", points);
                }
                else if (model.getDrawTool()==DrawTools.ELLIPSE){
                    PointCheck();
                    model.addCommand("ELLIPSE", points);
                }

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        gui.undoOption.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.undo();
            }
        });

        UndoListener undoListener= new UndoListener();
        gui.addKeyListener(undoListener);
        gui.polygonButton.addKeyListener(undoListener);
        gui.window.addKeyListener(undoListener);


        ColourListener paletteListener = new ColourListener();
        gui.paletteBlack.addActionListener(paletteListener);
        gui.paletteWhite.addActionListener(paletteListener);
        gui.paletteRed.addActionListener(paletteListener);
        gui.paletteGreen.addActionListener(paletteListener);
        gui.paletteBlue.addActionListener(paletteListener);

//        gui.testerButton.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                model.tester();
//            }
//        });
        gui.addComponentListener(new ComponentListener() {
            @Override
            public void componentResized(ComponentEvent e) {
                gui.c.ipady = gui.canvasFrame.getHeight();      //make this component tall
                gui.c.ipadx = gui.canvasFrame.getHeight();
                gui.canvasFrame.remove(gui.getCanvas());
                gui.canvasFrame.add(gui.getCanvas(), gui.c);

                //System.out.println("yes");
            }

            @Override
            public void componentMoved(ComponentEvent e) {

            }

            @Override
            public void componentShown(ComponentEvent e) {

            }

            @Override
            public void componentHidden(ComponentEvent e) {

            }
        });
    }

    private class ColourListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            Component source = (Component) e.getSource();
            penColor = source.getBackground();
            //System.out.println(penColor);
            //gui.penColorTile.setBackground(penColor);
            model.addColorCommand("PEN", penColor);
        }
    }

    private class UndoListener implements KeyListener {
        @Override
        public void keyTyped(KeyEvent e) {
            //dont need
        }

        @Override
        public void keyPressed(KeyEvent e) {
            //System.out.println("key");
            if (e.isControlDown() && e.getKeyCode()==KeyEvent.VK_Z){
                model.undo();
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
            // dont need
        }
    }

    private void PointCheck(){
        int tempX = start.x;
        int tempY = start.y;

        if (start.x > end.x){
            start.x = end.x;
            end.x = tempX;
        }
        if (start.y > end.y){
            start.y = end.y;
            end.y = tempY;
        }
    }

//    private class ColourListener implements ActionListener {
//        public void actionPerformed(ActionEvent e) {
//            Component source = (Component) e.getSource();
//            if (source == view.getRedButton() || source == view.getRedChoice()) {
//                model.changeRed();
//            } else if (source == view.getBlueButton()
//                    || source == view.getBlueChoice()) {
//                model.changeBlue();
//            } else if (source == view.getWhiteButton()
//                    || source == view.getWhiteChoice()) {
//                model.changeWhite();
//            }
//        }
//    }


}





package GUI;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.List;

/**
 * Responsible for housing and setting up the GUI components.
 * The class has no dependencies as it just describes the layout of the GUI.
 * Layout description includes arrangement, size, text and colour of the components.
 */
public class VecPaintGUI extends JFrame{
    public JPanel window;
    public JPanel bottomBar;
    private JPanel tools;
    private JPanel filePanel;
    public JButton lineTool;
    public JButton plotButton;
    public JButton rectangleButton;
    public JButton ellipseButton;
    public JButton penColourButton;
    public JPanel penColorTile;
    public JButton testerButton;
    public JCheckBox fillCheckBox;
    public JPanel fillColorTile;
    public JButton fillColorButton;
    public JButton paletteWhite;
    public JButton paletteBlack;
    public JButton paletteRed;
    public JButton paletteGreen;
    public JButton paletteBlue;
    public JLabel infoLabel;
    public JButton polygonButton;
    public JPanel canvasFrame;

    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenu editMenu;
    public JMenuItem openOption;
    public JMenuItem saveOption;
    public JMenuItem undoOption;
    private List<String> vecCommands;

    private VecPainter painter = new VecPainter();
    private Canvas vecCanvas;
    public GridBagConstraints c = new GridBagConstraints();

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

        private void setupMenuBar(){
        filePanel = new JPanel();
        menuBar = new JMenuBar();
        fileMenu = new JMenu("File");
        editMenu = new JMenu("Edit");
        openOption = new JMenuItem("Open");
        saveOption = new JMenuItem("Save");
        undoOption = new JMenuItem("Undo");
        infoLabel.setText(" ");
        //infoLabel.hide();

        window.add(filePanel);

        fileMenu.add(openOption);
        fileMenu.add(saveOption);
        editMenu.add(undoOption);

        menuBar.add(fileMenu);
        menuBar.add(editMenu);
        filePanel.add(menuBar);

        setJMenuBar(menuBar);


        }
    /**
     * Create a VecPaintGUI Object.
     */
    public VecPaintGUI() {

        setFocusable(true);
        setupMenuBar();
        add(window);
        setTitle("VecPaint");
        //setMinimumSize(new Dimension(600, 450));
        vecCanvas = new Canvas();
        vecCanvas.setFocusable(true);
        vecCanvas.setBackground(Color.WHITE);
//        JPanel canvasFrame = new JPanel();
        canvasFrame.setBackground(Color.BLACK);

        canvasFrame.setLayout(new GridBagLayout());

        canvasFrame.setSize(300,300);
        //vecCanvas.setLayout(new GridBagLayout());
        window.add(canvasFrame);
        //c.fill = GridBagConstraints.VERTICAL;
        c.ipady = canvasFrame.getHeight();
        c.ipadx = canvasFrame.getHeight();
        canvasFrame.add(vecCanvas, c);
        setFocusableComponents();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
    }
    /**
     * A JPanel that uses a VecPainter Object to draw on it.
     */
    public class Canvas extends JPanel {
        public void paint(Graphics g){
            super.paint(g);
            if (vecCommands != null) {
                painter.drawAll(g, vecCommands, this.getHeight(), this.getWidth());
                //setSize(new Dimension(300,300));
            }
        }
    }

    public Canvas getCanvas(){
            return vecCanvas;
    }

    public int[] getCanvasDim(){
        int[] dim = new int[2];
        dim[0] = vecCanvas.getWidth();
        dim[1] = vecCanvas.getHeight();
        return dim;
    }
    private void redrawCanvas(){
        vecCanvas.repaint();
    }
    private void setFocusableComponents(){
        for(Component comp : tools.getComponents()){
            comp.setFocusable(false);
        }
        for(Component comp : bottomBar.getComponents()){
            comp.setFocusable(false);
        }
        polygonButton.setFocusable(true);
    }

    /**
     * Redraw all shapes on the Canvas Object.
     * @param commands the new list of drawing commands to draw.
     */
    public void updateCanvas(List<String> commands){
        vecCommands = commands;
        redrawCanvas();
    }

}

package GUI;

import Controllers.GuiController;
import VecPaint.VecPaint;

public class Main {
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                VecPaintGUI vecPaintGUI = new VecPaintGUI();
                VecPaint vecPaint = new VecPaint(vecPaintGUI);
                GuiController guiController = new GuiController(vecPaintGUI, vecPaint);
                vecPaintGUI.setVisible(true);
            }
        });
    }
}

package GUI;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

public class VecPainter {
    private Color fillColor;
    private Color penColor;
    private boolean fill;

    /**
     *  Designed for use with a Janel. Its purpose is to provide a method which draws all the vec commands to a Graphics object in a Jpanel.
     *  It utilises the inbuilt java awt graphics methods for drawing shapes.
     */

    public VecPainter(){
    }

    /**
     *  Will execute all the given vec commands by drawing the appropriate shapes or setting colors.
     * @param g the graphics object from the JPanel to draw on.
     * @param vecCommands the list of all commands to execute.
     * @param height the current height of the JPanel in pixels.
     * @param width the current width of the Jpanel in pixels.
     */

    public void drawAll(Graphics g, List<String> vecCommands, int height, int width){
        //reset color and fill state
        penColor = Color.BLACK;
        fillColor = Color.WHITE;
        fill = false;

        // for each line(command) in the vec file, draw it
        for(String vecCommand : vecCommands) {
            // Pen color change command
            if (vecCommand.contains("PEN")) {
                getPenColor(vecCommand);
                //set pen color
                g.setColor(penColor);
            }
            // Turn on fill and change fill color command
            if (vecCommand.contains("FILL")) {
                if (vecCommand.contains("OFF")) { fill = false; }
                else {
                    fill = true;
                    getFillColor(vecCommand);
                }
            }
            // Draw a line command
            if (vecCommand.contains("LINE")) {
                double[] points = extractPoints(vecCommand);
                // draw the line, scale with the window
                g.drawLine((int)(points[0]*width), (int)(points[1]*height), (int)(points[2]*width), (int)(points[3]*height));
            }

            if (vecCommand.contains("PLOT")) {
                double[] points = extractPoints(vecCommand);
                // draw the line, scale with the window
                g.fillOval((int)(points[0]*width)-1, (int)(points[1]*height)-1, 3, 3);
            }

            // Draw a rectangle command
            if (vecCommand.contains("RECTANGLE")) {
                double[] points = extractPoints(vecCommand);
                double x1 = points[0];
                double y1 = points[1];
                double x2 = points[2];
                double y2 = points[3];
                // if fill is on then draw fill before outline
                if (fill) {
                    g.setColor(fillColor);
                    g.fillRect((int) (x1 * width), (int) (y1 * height), (int) ((x2 - x1) * width), (int) ((y2 - y1) * height));
                    g.setColor(penColor); //reset color to pen for next shape
                }

                // draw the rectangle outline, scale with the window
                g.drawRect((int) (x1 * width), (int) (y1 * height), (int) ((x2 - x1) * width), (int) ((y2 - y1) * height));
            }

            if (vecCommand.contains("ELLIPSE")) {
                double[] points = extractPoints(vecCommand);
                double x1 = points[0];
                double y1 = points[1];
                double x2 = points[2];
                double y2 = points[3];
                // if fill is on then draw fill before outline
                if (fill) {
                    g.setColor(fillColor);
                    g.fillOval((int) (x1 * width), (int) (y1 * height), (int) ((x2 - x1) * width), (int) ((y2 - y1) * height));
                    g.setColor(penColor); //reset color to pen for next shape
                }
                // draw the line, scale with the window
                g.drawOval((int) (x1 * width), (int) (y1 * height), (int) ((x2 - x1) * width), (int) ((y2 - y1) * height));
            }

            // Draw a polygon
            if (vecCommand.contains("POLYGON")) {
                int[][] xyPoints = extractPolyPoints(vecCommand, width, height);
                int[] xpoints = xyPoints[0];
                int[] ypoints = xyPoints[1];

                if (fill) {
                    g.setColor(fillColor);
                    g.fillPolygon(xpoints, ypoints, xpoints.length);
                    g.setColor(penColor); //reset color to pen for next shape
                }

                // draw the line, scale with the window
                g.drawPolygon(xpoints, ypoints, xpoints.length);
            }
        }

    }

    /**
     * @return The numbers after a command. eg. 'LINE 0.1 0.2 0.2 0.1' will return 0.1 0.2 0.2 0.1
     * @param vecCommand A single vec command (line from a vec file)
     */
    private double[] extractPoints(String vecCommand){
        String[] split = vecCommand.split(" ");
        //remove the starting command in order to extract
        String[] pointsString = Arrays.copyOfRange(split, 1, split.length);
        double[] points = new double[pointsString.length];
        for (int i = 0; i < points.length; i++){
            //Parses the integer for each string.
            points[i] = Double.parseDouble(pointsString[i]);
        }
        return points;
    }

    /**
     * @return all the points from a POLYGON command
     * @param vecCommand A single vec command (line from a vec file)
     * @param width Width in pixels of the JPanel, used for scaling.
    *  @param height Height in pixels of the JPanel, used for scaling.
     */
    private int[][] extractPolyPoints(String vecCommand, int width, int height){
        String[] split = vecCommand.split(" ");
        //remove the starting command in order to extract
        String[] pointsString = Arrays.copyOfRange(split, 1, split.length);
        int[][] xyPoints = new int[2][pointsString.length/2];
        for (int i = 0; i < pointsString.length-1; i+=2){
            //Parses the integer for each string.
            xyPoints[0][i/2] = (int)(Double.parseDouble(pointsString[i])*width);
            //System.out.println(xyPoints[0][i/2]);
        }
        for (int i = 1; i < pointsString.length; i+=2){
            //Parses the integer for each string.
            xyPoints[1][(i-1)/2] = (int)(Double.parseDouble(pointsString[i])*height);
            //System.out.println(xyPoints[1][i/2]);
        }

        return xyPoints;
    }

    private void getPenColor(String vecCommand){
        String[] split = vecCommand.split(" ");
        //remove the starting command in order to extract
        String[] color = Arrays.copyOfRange(split, 1, split.length);
        penColor = Color.decode(color[0]);
    }

    private void getFillColor(String vecCommand){
            String[] split = vecCommand.split(" ");
            //remove the starting command in order to extract
            String[] color = Arrays.copyOfRange(split, 1, split.length);
            fillColor = Color.decode(color[0]);
    }

}

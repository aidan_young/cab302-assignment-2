package VecPaint;

import Controllers.DrawTools;
import GUI.VecPaintGUI;
import VecFileManager.VecFile;
import java.awt.*;
import java.io.*;
import java.nio.file.Path;

/**
 * A model responsible for the application logic and behaviours.
 * It includes the various methods need to interface with the program and VecFile.
 */
public class VecPaint {
    private VecPaintGUI gui;
    private DrawTools drawTool;
    private VecFile vec = new VecFile();

    /**
     * @param vecGUI the GUI Object
     */
    public VecPaint(VecPaintGUI vecGUI){
        gui = vecGUI;
    }
    /**
     * @return the current drawing tool
     */
    public DrawTools getDrawTool(){
        return drawTool;
    }

    /**
     * Set the current drawing tool
     * @param tool the drawing tool
     */
    public void setDrawTool(DrawTools tool){
        drawTool = tool;
    }

    /**
     * Add a shape draw command to the list of vec commands, supports PLOT, LINE, RECTANGLE, ECLIPSE and POLYGON
     * @param commandName The command name eg. LINE.
     * @param points all the points that define the shape.
     */
    public void addCommand(String commandName, Point[] points){
        StringBuilder command = new StringBuilder();
        command.append(commandName);
        for(Point point : points) {
            double xScaled = (double) point.x / gui.getCanvasDim()[0];
            double yScaled = (double) point.y / gui.getCanvasDim()[1];
            xScaled = xScaled > 1 ? 1 : xScaled;
            yScaled = yScaled > 1 ? 1 : yScaled;
            xScaled = xScaled < 0 ? 0 : xScaled;
            yScaled = yScaled < 0 ? 0 : yScaled;
            command.append(String.format(" %.4f %.4f", xScaled, yScaled));
        }
        //System.out.println(command);
        vec.addCommand(command.toString());
        gui.updateCanvas(vec.toList());
    }
    /**
     * Add a color change command to the list of vec commands, supports PEN and FILL
     * @param commandName The command name eg. LINE.
     * @param color The new Colour
     */
    public void addColorCommand(String commandName, Color color){
        String command;
        String hex = String.format("#%02X%02X%02X", color.getRed(), color.getGreen(), color.getBlue());
        command = commandName + " " + hex;
        //System.out.println(command);
        if (vec.toList().isEmpty()){
            vec.addCommand(command);
        }
        else{
            String lastLine = (String)vec.toList().get(vec.toList().size()-1);
            if(lastLine.contains(commandName)){
                vec.removeLine();
                vec.addCommand(command);
            }
            else{
                vec.addCommand(command);
            }
        }
    }
    /**
     * Turn off Fill and adds the appropriate command to the command list.
     */
    public void fillOff(){
        String lastLine = (String)vec.toList().get(vec.toList().size()-1);
        if(lastLine.contains("FILL")){
            vec.undo();
            vec.addCommand("FILL OFF");
        }
        else{
            vec.addCommand("FILL OFF");
        }
    }
    /**
     * Overwrites the current VecFile with a new one and updates the GUI to draw the new file.
     * @param path the path to the .vec file.
     */
    public void open(Path path){
        vec = new VecFile(path);
        gui.updateCanvas(vec.toList());
    }

    /**
     * Save the current canvas (drawings) to a .vec file.
     * @param file The file to write to.
    */
    public void save(File file) throws IOException {
            if(!vec.toList().isEmpty()) {
                FileWriter fr = null;
                try {
                    fr = new FileWriter(file);
                    for(Object line : vec.toList()) {
                        fr.write((String) line + "\n");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }finally{
                    //close resources
                    try {
                        fr.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        }
    }
    /**
     * Remove the last drawing and update the screen.
     */
    public void undo(){
        vec.undo();
        gui.updateCanvas(vec.toList());
    }

}

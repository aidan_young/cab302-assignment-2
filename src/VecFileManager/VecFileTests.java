package VecFileManager;

import org.junit.jupiter.api.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class VecFileTests {
    VecFile vec;
    Path path = Paths.get(".\\src\\VecFileManager\\example2.vec");
    BufferedReader vecContents;
    @BeforeEach
    public void init() throws IOException {
        vec = new VecFile(path);

        vecContents = Files.newBufferedReader(path);
    }

    @Test
    public void constructorTest() throws IOException {
        List<String> expected = new LinkedList<>();
        String line = null;
        while ((line = vecContents.readLine()) != null) {
            expected.add(line);
            //System.out.println(line);
        }
        assertEquals(expected, vec.toList());
    }

    @Test
    public void getContentsTest() throws IOException {
        List<String> lines = vec.toList();
        for (String line : lines) {
            System.out.println(line);
        }
    }

}

package VecFileManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * A container for ‘.vec’ files, has useful methods for modifying and reading its contents.
 */

public class VecFile {
    private List<String> text = new LinkedList<>();
    /**
     *  Create an empty VecFile
     */
    public VecFile(){ }
    /**
     *  Create a VecFile by Loading a .vec file given a file path.
     * @param filePath the path to the vec file.
     */
    public VecFile(Path filePath){
        load(filePath);
    }

    /**
     *  Load a vec file. Will extract each line of the .vec file as a String in a List.
     * @param filePath the path to the vec file.
     */
    private void load(Path filePath){
        BufferedReader reader = null;
        try {
            reader = Files.newBufferedReader(filePath);

            String line = null;
            while ((line = reader.readLine()) != null) {
                text.add(line);
                //System.out.println(line);
            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
    }

    /**
     * @return the current list of vec commands.
     */
    public List toList(){
        return text;
    }

    /**
     *  Remove the last draw command from the list. Will ignore PEN and FILL commands as these are not drawings.
     */
    public void undo(){
        if(!text.isEmpty()) {
            // only remove draw commands
            for (int i = text.size()-1; i>=0; i--){
                String line = text.get(i);
                if(line.contains("FILL") || line.contains("PEN")){
                }
                else{
                    text.remove(i);
                    break;
                }
            }
        }
    }

    /**
     * Remove the last line in the vec file
     */
    public void removeLine(){
        text.remove(text.size()-1);
    }

    /**
     * Append a vec command to the current list of vec commands
     * @param command The command to append
     */
    public void addCommand(String command){
        text.add(command);
    }

}
